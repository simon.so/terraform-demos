provider "aws" {
  region = "us-west-2"
}

data "aws_ami" "amzn_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}


resource "aws_instance" "web" {
  ami = "${data.aws_ami.amzn_linux.id}"
  instance_type = "t2.micro"
  subnet_id = "subnet-aacd30f1"
}
